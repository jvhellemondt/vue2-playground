import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const views = require.context("@/views", false, /^(.)*\.vue$/);
const routes = [
  ...views.keys().map(key => ({
    path: `/${key.replace(/^\.\/([^.]+)\.vue$/, "$1")}`,
    name: key.replace(/^\.\/([^.]+)\.vue$/, "$1"),
    component: views(key).default
  })),
  {
    path: "/",
    redirect: "/Home"
  }
];

export default new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});
